#!/usr/bin/env python

"""
    Manipulate nftables rules
"""


def _reverse_mapping(mapping):
    """
    Reverse the mapping of a dictionnary
    from {'meta': ['iif', 'oif']} creates {'iif': 'meta', 'oif': 'meta'}
    """
    rmap = {}
    for key, items in mapping.items():
        for item in items:
            rmap.setdefault(item, set()).add(key)
    return rmap


class FilterModule:
    """
    Gives access to filters for jinja templating
    """

    def filters(self):
        """
        Returns filters associated with this object
        """
        return {
            "nft_create_str": self.nft_create_str,
            "nft_make_filter_rules": self.nft_make_filter_rules,
            "nft_make_nat_filter_rules": self.nft_make_nat_filter_rules,
            "nft_make_nat_rules": self.nft_make_nat_rules,
        }

    NFT_FILTER_MODULES = _reverse_mapping(
        {
            "ct": ["state"],
            "icmp": ["type"],
            "ip": ["protocol", "daddr", "saddr"],
            "meta": ["nfproto", "oif", "iif", "oifname", "iifname", "skuid", "skgid"],
            "tcp": ["dport", "sport"],
            "udp": ["dport", "sport"],
        }
    )

    NFT_NAT_STATEMENTS = [
        "accept",
        "dnat",
        "masquerade",
        "redirect",
        "snat",
    ]

    @staticmethod
    def nft_create_str(nf_variable):
        """
        Returns `nf_variable` string representation
        Ex : 'dport' -> 'dport'
        Ex : {'12' : None, '1243': None} -> '{12, 1243}'
        """
        if isinstance(nf_variable, dict):
            keys = nf_variable.keys()
            return "{" + ", ".join(str(item) for item in keys) + "}"

        return nf_variable

    def nft_make_filter_rules(self, rule, verdict):
        """
        From object rule creates nftables filter rule
        Ex : {'dport' : '22', 'daddr' : '$pub_cri', 'proto': 'tcp'}
        -> ip daddr $pub_cri tcp dport 22 accept
        """
        rules = []
        if rule.get("raw", None):
            return rule["raw"]
        protos = rule.get("proto")
        if rule.get("dport") or rule.get("sport"):
            protocols = set([protos]) if protos else set(["tcp", "udp"])
        else:
            protocols = set([protos]) if protos else set([None])
        for proto in sorted(protocols):
            result = []
            for key, value in rule.items():
                if key is None or value is None:
                    continue
                modules = self.NFT_FILTER_MODULES.get(key, set())
                if modules:
                    values = [value]
                    if isinstance(value, list):
                        values = value
                    for value in values:
                        result.append(
                            "{} {} {}".format(
                                proto if proto in modules else next(iter(modules)),
                                key,
                                self.nft_create_str(value),
                            )
                        )
            if result:
                result = list(sorted(result))
                result.append(verdict)
                rules.append(" ".join([str(x) for x in result]))
        return "\n    ".join([str(x) for x in rules])

    def nft_make_nat_rules(self, rule):
        """
        From object rule creates nftables nat rule
        Ex : {'dport' : '22', 'daddr' : '$pub_cri', 'proto': 'tcp', 'dnat': 'cri'}
        -> ip daddr $pub_cri tcp dport 22 dnat to cri
        """
        rules = []
        for natst in self.NFT_NAT_STATEMENTS:
            if natst in rule:
                rules.append(
                    self.nft_make_filter_rules(
                        rule,
                        "{} to {}".format(natst, rule[natst]),
                    )
                )
            elif rule.get("action", "") == natst:
                rules.append(
                    self.nft_make_filter_rules(
                        rule,
                        "{}".format(natst),
                    )
                )
        return "\n".join([str(x) for x in rules])

    def nft_make_nat_filter_rules(self, rule):
        """
        From object rule creates nftables nat filter rule
        Ex : {'dport' : '22', 'daddr' : '$pub_cri', 'proto': 'tcp', 'dnat': 'cri'}
        -> ip daddr cri tcp dport 22 accept
        """
        rules = []
        rule = rule.copy()
        if "dnat" in rule:
            daddr, dport, *_ = list(rule["dnat"].split(":")) + [rule.get("dport", None)]
            rule["daddr"] = daddr
            rule["dport"] = dport
            rules.append(self.nft_make_filter_rules(rule, "accept"))
        return "\n".join([str(x) for x in rules])
